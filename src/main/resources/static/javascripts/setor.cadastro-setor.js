$(function($) {

	var modal = $('#modalCadastroRapidoSetor');
	var botaoSalvar = modal.find('.js-modal-cadastro-setor-salvar-btn');
	var form = modal.find('form');
	form.on('submit', function(event) {
		event.preventDefault()
	});
	var url = form.attr('action');
	var inputNomeEstilo = $('#nomeSetor');
	var containerMensagemErro = $('.js-mensagem-cadastro-rapido-setor');

	modal.on('shown.bs.modal', onModalShow);
	modal.on('hide.bs.modal', onModalClose)
	botaoSalvar.on('click', onBotaoSalvarClick);

	function onModalShow() {
		inputNomeSetor.focus();
	}

	function onModalClose() {
		inputNomeSetor.val('');
		containerMensagemErro.addClass('hidden');
		form.find('.form-group').removeClass('has-error');
	}

	function onBotaoSalvarClick() {
		var nomeSetor = inputNomeSetor.val().trim();
		$.ajax({
			url : url,
			method : 'POST',
			contentType : 'application/json',
			data : JSON.stringify({
				nome : nomeSetor
			}),
			error : onErroSalvandoSetor,
			success : onSetorSalvo
		});
	}

	function onErroSalvandoSetor(obj) {
		var mensagemErro = obj.responseText;
		containerMensagemErro.removeClass('hidden');
		containerMensagemErro.html('<span>' + mensagemErro + '</span>');
		form.find('.form-group').addClass('has-error');
	}

	function onSetorSalvo(estilo) {
		atualizarCombo()
		hideModal();
	}

	function hideModal() {
		$(".modal").removeClass("in");
		$(".modal-backdrop").remove();
		$('body').removeClass('modal-open');
		$('body').css('padding-right', '');
		$(".modal").hide();
	}

	function atualizarCombo() {
		var nomeEstilo = inputNomeEstilo.val().trim();
		$.ajax({
			url : "http://localhost:8081/cervejeiro/estilo",
			method : 'GET',
			contentType : 'application/json',
			error : onErroSalvandoEstilo,
			success : function(estilos) {
				var comboEstilo = $('#estilo');
				comboEstilo.html(' ');
				var opts = JSON.parse(JSON.stringify(estilos));
				$.each(opts, function(i, estilo) {
					comboEstilo.append('<option value=' + estilo.codigo + '>'
							+ estilo.nome + '</option>');
				});
			}
		});
	}

});