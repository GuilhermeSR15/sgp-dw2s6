package br.edu.ifsp.arq.dw2.sgp.repository.filter;

public class GruposFilter {

	private Long id;
	private String nome;
	
	public Long getId() {
		return id;
	}
	public void setCodigo(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
