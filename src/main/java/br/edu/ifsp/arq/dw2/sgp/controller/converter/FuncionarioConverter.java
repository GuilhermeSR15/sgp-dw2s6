package br.edu.ifsp.arq.dw2.sgp.controller.converter;
import org.springframework.core.convert.converter.Converter;
import org.thymeleaf.util.StringUtils;

import br.edu.ifsp.arq.dw2.sgp.model.Funcionario;
public class FuncionarioConverter implements Converter<String, Funcionario>{

	@Override
	public Funcionario convert(String codigo) {
		if (!StringUtils.isEmpty(codigo)) {
			Funcionario func = new Funcionario();
			func.setId(Long.valueOf(codigo));
			return func;
		}

		return null;
	}
	
	

}
