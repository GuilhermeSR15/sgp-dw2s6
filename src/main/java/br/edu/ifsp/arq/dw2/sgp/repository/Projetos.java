package br.edu.ifsp.arq.dw2.sgp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.ifsp.arq.dw2.sgp.model.Projeto;
import br.edu.ifsp.arq.dw2.sgp.repository.helper.projeto.ProjetosQueries;

@Repository
public interface Projetos extends JpaRepository<Projeto	, Long>, ProjetosQueries {
	public Optional<Projeto> findByNomeIgnoreCase(String nome);
}
