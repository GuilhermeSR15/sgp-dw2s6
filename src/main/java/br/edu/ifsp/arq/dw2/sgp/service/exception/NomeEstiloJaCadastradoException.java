package br.edu.ifsp.arq.dw2.sgp.service.exception;

public class NomeEstiloJaCadastradoException extends Exception {
	private static final long serialVersionUID = 1L;

	public NomeEstiloJaCadastradoException(String message) {
		super(message);
	}
}
