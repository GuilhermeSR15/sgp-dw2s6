package br.edu.ifsp.arq.dw2.sgp.repository.filter;

import java.math.BigDecimal;

import br.edu.ifsp.arq.dw2.sgp.model.Grupo;
import br.edu.ifsp.arq.dw2.sgp.model.Setor;

public class FuncionariosFilter {

	private Long id;
	private String nome;
	private String cpf;
	private String nomeUsuario;
	private String email;
	private Setor setor;
	private Grupo grupo;
	private BigDecimal salarioDe;
	private BigDecimal salarioAte;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getNomeUsuario() {
		return nomeUsuario;
	}
	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Setor getSetor() {
		return setor;
	}
	public void setSetor(Setor setor) {
		this.setor = setor;
	}
	public Grupo getGrupo() {
		return grupo;
	}
	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}
	public BigDecimal getSalarioDe() {
		return salarioDe;
	}
	public void setSalarioDe(BigDecimal salarioDe) {
		this.salarioDe = salarioDe;
	}
	public BigDecimal getSalarioAte() {
		return salarioAte;
	}
	public void setSalarioAte(BigDecimal salarioAte) {
		this.salarioAte = salarioAte;
	}
	
	
}
