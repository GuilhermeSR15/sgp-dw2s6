package br.edu.ifsp.arq.dw2.sgp.service.exception;

public class ConfirmSenhaObrigatoriaUsuarioException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ConfirmSenhaObrigatoriaUsuarioException(String message) {
		super(message);
	}

}
