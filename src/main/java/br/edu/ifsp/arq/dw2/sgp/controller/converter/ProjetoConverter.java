package br.edu.ifsp.arq.dw2.sgp.controller.converter;
import org.springframework.core.convert.converter.Converter;
import org.thymeleaf.util.StringUtils;

import br.edu.ifsp.arq.dw2.sgp.model.Projeto;
public class ProjetoConverter implements Converter<String, Projeto>{

	@Override
	public Projeto convert(String codigo) {
		if (!StringUtils.isEmpty(codigo)) {
			Projeto projeto = new Projeto();
			projeto.setId(Long.valueOf(codigo));
			return projeto;
		}

		return null;
	}
	
	

}
