package br.edu.ifsp.arq.dw2.sgp.controller.handler;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.edu.ifsp.arq.dw2.sgp.service.exception.NomeEstiloJaCadastradoException;

@ControllerAdvice
public class ControllerAdviceExceptionHandler {
	
	@ExceptionHandler(NomeEstiloJaCadastradoException.class)
	public ResponseEntity<String> handlerNomeEstiloJaCadastradoException(
			NomeEstiloJaCadastradoException exception) {
		return ResponseEntity.badRequest().body(exception.getMessage());
	}
}
