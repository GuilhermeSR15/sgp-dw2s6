package br.edu.ifsp.arq.dw2.sgp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.ifsp.arq.dw2.sgp.controller.page.PageWrapper;
import br.edu.ifsp.arq.dw2.sgp.model.Projeto;
import br.edu.ifsp.arq.dw2.sgp.repository.Projetos;
import br.edu.ifsp.arq.dw2.sgp.repository.filter.ProjetosFilter;
import br.edu.ifsp.arq.dw2.sgp.service.ProjetoService;
import br.edu.ifsp.arq.dw2.sgp.service.SetorService;
import br.edu.ifsp.arq.dw2.sgp.service.exception.ImpossivelExcluirEntidadeException;
import br.edu.ifsp.arq.dw2.sgp.service.exception.NomeProjetoJaCadastradoException;


@Controller
@RequestMapping("/projeto")
public class ProjetosController {
	@Autowired
	private Projetos projetos;
	
	@Autowired
	private ProjetoService projetoService;
	
	@Autowired SetorService setorService;


	@RequestMapping("/novo")
	public ModelAndView novo(Projeto projeto) {
		ModelAndView mv = new ModelAndView("projeto/cadastro-projeto");
		mv.addObject("setores", setorService.listarTodos());
		return mv;
	}

	@RequestMapping(value = "/novo", method = RequestMethod.POST)
	public ModelAndView cadastrar(@Valid Projeto projeto, BindingResult result, Model model,
			RedirectAttributes attributes) {
		if (result.hasErrors()) {
			return novo(projeto);
		}
		try {
			projetoService.salvar(projeto);
		} catch (NomeProjetoJaCadastradoException e) {
			result.rejectValue("nome", e.getMessage(), e.getMessage());
			return novo(projeto);
		}

		attributes.addFlashAttribute("mensagem", "Projeto salvo com sucesso");
		return new ModelAndView("redirect:/projeto/novo");
	}

	@RequestMapping(value = "/pesquisar", method = RequestMethod.GET)
	public ModelAndView pesquisar(ProjetosFilter projetosFilter, BindingResult result,
			@PageableDefault(size = 2) Pageable pageable, 
			HttpServletRequest httpServletRequest) {
		ModelAndView mv = new ModelAndView("projeto/pesquisa-projeto");

		PageWrapper<Projeto> paginaWrapper = new PageWrapper<>(projetos.filtrar(projetosFilter, pageable),
				httpServletRequest);
		mv.addObject("pagina", paginaWrapper);
		return mv;
	}
	
	@DeleteMapping("/{codigo}")
	public @ResponseBody ResponseEntity<?> excluir(@PathVariable("codigo") Projeto projeto) {
		try {
			projetoService.excluir(projeto);
		} catch (ImpossivelExcluirEntidadeException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(value = "/editar/{codigo}")
	public ModelAndView editar(@PathVariable("codigo") Projeto projeto) {
		ModelAndView mv = new ModelAndView("projeto/cadastro-projeto");
		Projeto p = projetoService.buscar(projeto.getId());
		mv.addObject(p);
		mv.addObject("setores", setorService.listarTodos());
		return mv;
	}
	
	@RequestMapping(value = "/editar/{codigo}", method = RequestMethod.POST)
	public ModelAndView editar(@Valid Projeto projeto, BindingResult result, Model model,
			RedirectAttributes attributes) {
		if (result.hasErrors()) {
			return novo(projeto);
		}
		try {
			projetoService.salvar(projeto);
		} catch (NomeProjetoJaCadastradoException e) {
			result.rejectValue("nome", e.getMessage(), e.getMessage());
			return novo(projeto);
		}
		attributes.addFlashAttribute("mensagem", "Projeto alterado com sucesso");
		return new ModelAndView("redirect:/projeto/novo");
	}
}
