package br.edu.ifsp.arq.dw2.sgp.repository.helper.projeto_funcionario;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.edu.ifsp.arq.dw2.sgp.model.ProjetoFuncionario;
import br.edu.ifsp.arq.dw2.sgp.repository.filter.ProjetosFuncionariosFilter;

public interface ProjetosFuncionariosQueries {
	public Page<ProjetoFuncionario> filtrar(ProjetosFuncionariosFilter filtro, Pageable pageable);
}
