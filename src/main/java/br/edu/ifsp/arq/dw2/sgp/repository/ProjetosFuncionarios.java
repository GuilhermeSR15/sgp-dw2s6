package br.edu.ifsp.arq.dw2.sgp.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.ifsp.arq.dw2.sgp.model.Funcionario;
import br.edu.ifsp.arq.dw2.sgp.model.Projeto;
import br.edu.ifsp.arq.dw2.sgp.model.ProjetoFuncionario;
import br.edu.ifsp.arq.dw2.sgp.repository.helper.projeto_funcionario.ProjetosFuncionariosQueries;

@Repository
public interface ProjetosFuncionarios extends JpaRepository<ProjetoFuncionario, Long>, ProjetosFuncionariosQueries {
	public Optional<List<ProjetoFuncionario>> findByFuncionario (Funcionario funcionario);
	public Optional<List<ProjetoFuncionario>> findByProjeto (Projeto projeto);
	public Optional<ProjetoFuncionario> findByProjetoAndFuncionario(Projeto projeto, Funcionario funcionario);
}
