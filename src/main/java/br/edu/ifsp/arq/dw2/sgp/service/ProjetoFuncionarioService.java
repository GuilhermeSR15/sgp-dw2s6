package br.edu.ifsp.arq.dw2.sgp.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.PersistenceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.edu.ifsp.arq.dw2.sgp.dto.ProjetoFuncionarioDto;
import br.edu.ifsp.arq.dw2.sgp.model.Funcionario;
import br.edu.ifsp.arq.dw2.sgp.model.Projeto;
import br.edu.ifsp.arq.dw2.sgp.model.ProjetoFuncionario;
import br.edu.ifsp.arq.dw2.sgp.repository.ProjetosFuncionarios;
import br.edu.ifsp.arq.dw2.sgp.service.exception.FuncionarioJaAlocadoNoProjeto;
import br.edu.ifsp.arq.dw2.sgp.service.exception.ImpossivelExcluirEntidadeException;

@Service
public class ProjetoFuncionarioService {
	@Autowired
	private ProjetosFuncionarios projetosFuncionarios;
	
	
	@Autowired
	private FuncionarioService funcionarioService;

	@Transactional
	public void salvar(ProjetoFuncionarioDto projetoFuncionarioDto) throws FuncionarioJaAlocadoNoProjeto {
		long idProjeto = projetoFuncionarioDto.getProjeto().getId();
		List<Funcionario> funcionarios = projetoFuncionarioDto.getFuncionarios();
		int tamanhoLista = funcionarios.size();

		ProjetoFuncionario pf = null;

		for (int i = 0; i < tamanhoLista; i++) {
			pf = new ProjetoFuncionario();

			pf.setProjeto(new Projeto(idProjeto));
			pf.setFuncionario(new Funcionario(funcionarios.get(i).getId()));
			pf.setCargaHoraria(projetoFuncionarioDto.getCargaHoraria());
			pf.setInicioParticipacao(projetoFuncionarioDto.getInicioParticipacao());
			pf.setFimParticipacao(projetoFuncionarioDto.getFimParticipacao());
			pf.setGestor(projetoFuncionarioDto.getGestor());
			
			this.validarEsalvar(pf);
		}
	}
	
	private void validarEsalvar(ProjetoFuncionario pf) throws FuncionarioJaAlocadoNoProjeto  {
		Optional<ProjetoFuncionario> pfOptional = projetosFuncionarios
				.findByProjetoAndFuncionario(pf.getProjeto(), pf.getFuncionario());
		if (pfOptional.isPresent()) {
			pf.setFuncionario(funcionarioService.buscar(pf.getFuncionario().getId()));
			throw new FuncionarioJaAlocadoNoProjeto("Funcionário " + pf.getFuncionario().getNome() + " já esta alocado no projeto");
		}
		projetosFuncionarios.saveAndFlush(pf);
	}

	public List<ProjetoFuncionario> listarTodos() {
		return projetosFuncionarios.findAll();
	}

	public ProjetoFuncionario buscar(Long id) {
		return projetosFuncionarios.findOne(id);
	}

	@Transactional
	public void excluir(ProjetoFuncionario projetoFuncionario) {
		try {
			projetosFuncionarios.delete(projetoFuncionario);
			projetosFuncionarios.flush();
		} catch (PersistenceException e) {
			throw new ImpossivelExcluirEntidadeException(
					"Impossível apagar projeto. Está vinculado a alguma entidade.");
		}

	}
}
