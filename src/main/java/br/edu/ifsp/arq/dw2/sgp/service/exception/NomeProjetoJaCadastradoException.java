package br.edu.ifsp.arq.dw2.sgp.service.exception;

public class NomeProjetoJaCadastradoException extends Exception {
	private static final long serialVersionUID = 1L;

	public NomeProjetoJaCadastradoException(String message) {
		super(message);
	}
}
