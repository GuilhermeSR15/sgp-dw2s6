package br.edu.ifsp.arq.dw2.sgp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.ifsp.arq.dw2.sgp.controller.page.PageWrapper;
import br.edu.ifsp.arq.dw2.sgp.model.Grupo;
import br.edu.ifsp.arq.dw2.sgp.repository.Grupos;
import br.edu.ifsp.arq.dw2.sgp.repository.filter.GruposFilter;
import br.edu.ifsp.arq.dw2.sgp.service.GrupoService;
import br.edu.ifsp.arq.dw2.sgp.service.exception.ImpossivelExcluirEntidadeException;
import br.edu.ifsp.arq.dw2.sgp.service.exception.NomeGrupoJaCadastradoException;

@Controller
@RequestMapping("/grupo")
public class GruposController {

	@Autowired
	private Grupos grupos;

	@Autowired
	private GrupoService grupoService;

	@RequestMapping("/novo")
	public ModelAndView novo(Grupo grupo) {
		ModelAndView mv = new ModelAndView("grupo/cadastro-grupo");
		return mv;
	}

	@RequestMapping(value = "/novo", method = RequestMethod.POST)
	public ModelAndView cadastrar(@Valid Grupo grupo, BindingResult result, Model model,
			RedirectAttributes attributes) {
		if (result.hasErrors()) {
			return novo(grupo);
		}
		try {
			grupoService.salvar(grupo);
		} catch (NomeGrupoJaCadastradoException e) {
			result.rejectValue("nome", e.getMessage(), e.getMessage());
			return novo(grupo);
		}

		attributes.addFlashAttribute("mensagem", "Grupo salvo com sucesso");
		return new ModelAndView("redirect:/grupo/novo");
	}

	@RequestMapping(value = "/pesquisar", method = RequestMethod.GET)
	public ModelAndView pesquisar(GruposFilter gruposFilter, BindingResult result,
			@PageableDefault(size = 2) Pageable pageable, HttpServletRequest httpServletRequest) {
		ModelAndView mv = new ModelAndView("grupo/pesquisa-grupo");

		PageWrapper<Grupo> paginaWrapper = new PageWrapper<>(grupos.filtrar(gruposFilter, pageable),
				httpServletRequest);
		mv.addObject("pagina", paginaWrapper);
		return mv;
	}

	@DeleteMapping("/{codigo}")
	public @ResponseBody ResponseEntity<?> excluir(@PathVariable("codigo") Grupo grupo) {
		try {
			grupoService.excluir(grupo);
		} catch (ImpossivelExcluirEntidadeException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = "/editar/{codigo}")
	public ModelAndView editar(@PathVariable("codigo") Grupo grupo) {
		ModelAndView mv = new ModelAndView("grupo/cadastro-grupo");
		mv.addObject(grupoService.buscar(grupo.getId()));
		return mv;
	}

	@RequestMapping(value = "/editar/{codigo}", method = RequestMethod.POST)
	public ModelAndView editar(@Valid Grupo grupo, BindingResult result, Model model, RedirectAttributes attributes) {
		if (result.hasErrors()) {
			return novo(grupo);
		}
		try {
			grupoService.salvar(grupo);
			attributes.addFlashAttribute("mensagem", "Grupo alterado com sucesso");
			return new ModelAndView("redirect:/grupo/novo");
		} catch (NomeGrupoJaCadastradoException e) {
			result.rejectValue("nome", e.getMessage(), e.getMessage());
			return novo(grupo);
		}

	}

}
