package br.edu.ifsp.arq.dw2.sgp.repository.helper.funcionario;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.edu.ifsp.arq.dw2.sgp.model.Funcionario;
import br.edu.ifsp.arq.dw2.sgp.repository.filter.FuncionariosFilter;

public interface FuncionariosQueries {

	public Optional<Funcionario> porEmailEAtivo(String email);

//	public List<String> permissoes(Funcionario funcionario);
	
	public Page<Funcionario> filtrar(FuncionariosFilter filtro, Pageable pageable);

}
