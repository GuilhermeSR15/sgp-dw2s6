package br.edu.ifsp.arq.dw2.sgp.repository.filter;

import java.time.LocalDate;

import br.edu.ifsp.arq.dw2.sgp.model.Funcionario;
import br.edu.ifsp.arq.dw2.sgp.model.Projeto;

public class ProjetosFuncionariosFilter {
	private Long id;
	private Integer cargaHoraria;
	private Boolean gestor;
	private LocalDate inicioParticipacao;
	private LocalDate fimParticipacao;
	private Projeto projeto;
	private Funcionario funcionario;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCargaHoraria() {
		return cargaHoraria;
	}

	public void setCargaHoraria(Integer cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}

	public Boolean getGestor() {
		return gestor;
	}

	public void setGestor(Boolean gestor) {
		this.gestor = gestor;
	}

	public LocalDate getInicioParticipacao() {
		return inicioParticipacao;
	}

	public void setInicioParticipacao(LocalDate inicioParticipacao) {
		this.inicioParticipacao = inicioParticipacao;
	}

	public LocalDate getFimParticipacao() {
		return fimParticipacao;
	}

	public void setFimParticipacao(LocalDate fimParticipacao) {
		this.fimParticipacao = fimParticipacao;
	}

	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

}
