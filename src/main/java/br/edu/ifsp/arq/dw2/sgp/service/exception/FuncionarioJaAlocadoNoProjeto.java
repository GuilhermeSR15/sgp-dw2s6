package br.edu.ifsp.arq.dw2.sgp.service.exception;

public class FuncionarioJaAlocadoNoProjeto extends Exception {
	private static final long serialVersionUID = 1L;

	public FuncionarioJaAlocadoNoProjeto(String message) {
		super(message);
	}

}
