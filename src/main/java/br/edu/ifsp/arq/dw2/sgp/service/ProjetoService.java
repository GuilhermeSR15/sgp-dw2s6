package br.edu.ifsp.arq.dw2.sgp.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.PersistenceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.edu.ifsp.arq.dw2.sgp.model.Projeto;
import br.edu.ifsp.arq.dw2.sgp.repository.Projetos;
import br.edu.ifsp.arq.dw2.sgp.service.exception.ImpossivelExcluirEntidadeException;
import br.edu.ifsp.arq.dw2.sgp.service.exception.NomeProjetoJaCadastradoException;

@Service
public class ProjetoService {
	@Autowired
	private Projetos projetos;

	@Transactional
	public Projeto salvar(Projeto projeto) throws NomeProjetoJaCadastradoException {
		
		if(projeto.getId() == null) {
			Optional<Projeto> projetoOptional = projetos.findByNomeIgnoreCase(projeto.getNome());
			
			if (projetoOptional.isPresent()) {
				throw new NomeProjetoJaCadastradoException("Nome do projeto já cadastrado");
			}
		}
		
		return projetos.saveAndFlush(projeto);
	}
	
	public List<Projeto> listarTodos() {
		return projetos.findAll();
	}
	
	public Projeto buscar(Long id) {
		return projetos.findOne(id);
	}
	
	@Transactional
	public void excluir(Projeto projeto) {
		try {
			projetos.delete(projeto);
			projetos.flush();
		} catch (PersistenceException e) {
			throw new ImpossivelExcluirEntidadeException(
				"Impossível apagar projeto. Está vinculado a alguma entidade."
			);
		}
		
	}
}
