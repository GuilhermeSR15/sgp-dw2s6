package br.edu.ifsp.arq.dw2.sgp.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.PersistenceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.edu.ifsp.arq.dw2.sgp.model.Funcionario;
import br.edu.ifsp.arq.dw2.sgp.repository.Funcionarios;
import br.edu.ifsp.arq.dw2.sgp.service.exception.ConfirmSenhaObrigatoriaUsuarioException;
import br.edu.ifsp.arq.dw2.sgp.service.exception.EmailUsuarioJaCadastradoException;
import br.edu.ifsp.arq.dw2.sgp.service.exception.ImpossivelExcluirEntidadeException;
import br.edu.ifsp.arq.dw2.sgp.service.exception.SenhaObrigatoriaUsuarioException;

@Service
public class FuncionarioService {

	@Autowired
	private Funcionarios funcionarios;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Transactional
	public void salvar(Funcionario funcionario) {
		Optional<Funcionario> usuarioExistente = funcionarios.findByEmail(funcionario.getEmail());

		if (usuarioExistente.isPresent() && usuarioExistente.get().getId() != funcionario.getId()) {
			throw new EmailUsuarioJaCadastradoException("E-mail já cadastrado");
		}

		if (funcionario.isNovo() && StringUtils.isEmpty(funcionario.getSenha())) {
			throw new SenhaObrigatoriaUsuarioException("Senha é obrigatória para novo usuário");
		}
		else if (funcionario.isNovo() && StringUtils.isEmpty(funcionario.getConfirmacaoSenha())) {
			throw new ConfirmSenhaObrigatoriaUsuarioException("Confirmar senha é obrigatório");
		}
		else if (!StringUtils.isEmpty(funcionario.getSenha())
				&& StringUtils.isEmpty(funcionario.getConfirmacaoSenha())) {
			throw new ConfirmSenhaObrigatoriaUsuarioException("Confirmar senha é obrigatório");
		}

		if (funcionario.isNovo()) {
			funcionario.setSenha(this.passwordEncoder.encode(funcionario.getSenha()));
			funcionario.setConfirmacaoSenha(funcionario.getSenha());
			funcionarios.saveAndFlush(funcionario);
		}
		else {
			editar(funcionario);
		}

	}
	
	public List<Funcionario> listarTodos() {
		return funcionarios.findAll();
	}

	private void editar(Funcionario funcionario) {
		Funcionario findOne = funcionarios.findOne(funcionario.getId());

		findOne.setContentType("");
		findOne.setFoto("");
		
		findOne.setAtivo(funcionario.getAtivo());
		findOne.setCpf(funcionario.getCpf());
		findOne.setEmail(funcionario.getEmail());
		findOne.setGrupo(funcionario.getGrupo());
		findOne.setSetor(funcionario.getSetor());
		findOne.setSalario(funcionario.getSalario());
		findOne.setProjetoFuncionario(funcionario.getProjetoFuncionario());
		findOne.setNomeUsuario(funcionario.getNomeUsuario());
		findOne.setNome(funcionario.getNome());

		if (funcionario.getSenha() != null && !funcionario.getSenha().isEmpty()) {
			findOne.setSenha(this.passwordEncoder.encode(funcionario.getSenha()));

		}
		findOne.setConfirmacaoSenha(findOne.getSenha());
		
		funcionarios.saveAndFlush(findOne);
	}

	public Funcionario buscar(Long id) {
		return funcionarios.findOne(id);
	}

	@Transactional
	public void excluir(Funcionario funcionario) {
		try {
			funcionarios.delete(funcionario);
			funcionarios.flush();
		}
		catch (PersistenceException e) {
			throw new ImpossivelExcluirEntidadeException("Impossível apagar funcionário.");
		}

	}
}