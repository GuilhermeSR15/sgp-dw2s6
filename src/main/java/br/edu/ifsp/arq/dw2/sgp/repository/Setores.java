package br.edu.ifsp.arq.dw2.sgp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.ifsp.arq.dw2.sgp.model.Setor;
import br.edu.ifsp.arq.dw2.sgp.repository.helper.setor.SetoresQueries;

@Repository
public interface Setores extends JpaRepository<Setor, Long>, SetoresQueries {
	public Optional<Setor> findByNomeIgnoreCase(String nome);
}
