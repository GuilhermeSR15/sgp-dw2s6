package br.edu.ifsp.arq.dw2.sgp.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.PersistenceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.edu.ifsp.arq.dw2.sgp.model.Grupo;
import br.edu.ifsp.arq.dw2.sgp.repository.Grupos;
import br.edu.ifsp.arq.dw2.sgp.service.exception.ImpossivelExcluirEntidadeException;
import br.edu.ifsp.arq.dw2.sgp.service.exception.NomeGrupoJaCadastradoException;

@Service
public class GrupoService {
	@Autowired
	private Grupos grupos;

	@Transactional
	public Grupo salvar(Grupo grupo) throws NomeGrupoJaCadastradoException {
		Optional<Grupo> grupoOptional = grupos.findByNomeIgnoreCase(grupo.getNome());
		if (grupoOptional.isPresent()) {
			throw new NomeGrupoJaCadastradoException("Nome do grupo já cadastrado");
		}

		return grupos.saveAndFlush(grupo);
	}
	
	public List<Grupo> listarTodos() {
		return grupos.findAll();
	}
	
	public Grupo buscar(Long id) {
		return grupos.findOne(id);
	}
	
	@Transactional
	public void excluir(Grupo grupo) {
		try {
			grupos.delete(grupo);
			grupos.flush();
		} catch (PersistenceException e) {
			throw new ImpossivelExcluirEntidadeException(
				"Impossível apagar grupo. Está vinculado a algum funcionário"
			);
		}
		
	}
}
