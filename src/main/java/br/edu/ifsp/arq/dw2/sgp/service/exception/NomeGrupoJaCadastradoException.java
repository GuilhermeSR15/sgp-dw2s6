package br.edu.ifsp.arq.dw2.sgp.service.exception;

public class NomeGrupoJaCadastradoException extends Exception {
	private static final long serialVersionUID = 1L;

	public NomeGrupoJaCadastradoException(String message) {
		super(message);
	}
}
