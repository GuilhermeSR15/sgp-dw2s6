package br.edu.ifsp.arq.dw2.sgp.service.exception;

public class NomeSetorJaCadastradoException extends Exception {
	private static final long serialVersionUID = 1L;

	public NomeSetorJaCadastradoException(String message) {
		super(message);
	}
}
