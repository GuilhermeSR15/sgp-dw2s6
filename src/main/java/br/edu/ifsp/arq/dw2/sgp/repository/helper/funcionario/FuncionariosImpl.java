package br.edu.ifsp.arq.dw2.sgp.repository.helper.funcionario;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.edu.ifsp.arq.dw2.sgp.model.Funcionario;
import br.edu.ifsp.arq.dw2.sgp.repository.filter.FuncionariosFilter;

public class FuncionariosImpl implements FuncionariosQueries {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public Optional<Funcionario> porEmailEAtivo(String email) {

		String string = new StringBuilder()	.append(" from ")
											.append(Funcionario.class.getSimpleName())
											.append(" where lower(email) = lower(:email) ")
											.append(" and ativo = true")
											.toString();

		return manager	.createQuery(string, Funcionario.class)
						.setParameter("email", email)
						.getResultList()
						.stream()
						.findFirst();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public Page<Funcionario> filtrar(FuncionariosFilter filtro, Pageable pageable) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Funcionario.class);

		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistro = paginaAtual * totalRegistrosPorPagina;

		criteria.setFirstResult(primeiroRegistro);
		criteria.setMaxResults(totalRegistrosPorPagina);

		Sort sort = pageable.getSort();
		if (sort != null) {
			Sort.Order order = sort.iterator().next();
			String property = order.getProperty();
			criteria.addOrder(order.isAscending() ? Order.asc(property) : Order.desc(property));
		}

		adicionarFiltro(filtro, criteria);
		return new PageImpl<>(criteria.list(), pageable, total(filtro));
	}

	private Long total(FuncionariosFilter filtro) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Funcionario.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}

	private void adicionarFiltro(FuncionariosFilter filtro, Criteria criteria) {
		if (filtro != null) {
			if (filtro.getId() != null) {
				criteria.add(Restrictions.eq("id", filtro.getId()));
			}
			
			if (!StringUtils.isEmpty(filtro.getNome())) {
				criteria.add(Restrictions.ilike("nome", filtro.getNome(), MatchMode.ANYWHERE));
			}
			
			if (!StringUtils.isEmpty(filtro.getCpf())) {
				criteria.add(Restrictions.ilike("cpf", filtro.getCpf(), MatchMode.ANYWHERE));
			}
			
			if (!StringUtils.isEmpty(filtro.getNomeUsuario())) {
				criteria.add(Restrictions.ilike("nomeUsuario", filtro.getNomeUsuario(), MatchMode.ANYWHERE));
			}
			
			if (!StringUtils.isEmpty(filtro.getEmail())) {
				criteria.add(Restrictions.ilike("email", filtro.getEmail(), MatchMode.ANYWHERE));
			}
			
			if (filtro.getGrupo() != null && filtro.getGrupo().getId() != null) {
				criteria.add(Restrictions.eq("grupo", filtro.getGrupo()));
			}
			
			if (filtro.getSetor() != null && filtro.getSetor().getId() != null) {
				criteria.add(Restrictions.eq("setor", filtro.getSetor()));
			}
			
			if (filtro.getSalarioDe() != null) {
				criteria.add(Restrictions.ge("salario", filtro.getSalarioDe()));
			}

			if (filtro.getSalarioAte() != null) {
				criteria.add(Restrictions.le("salario", filtro.getSalarioAte()));
			}
		}
	}

	// @Override
	// public List<String> permissoes(Funcionario user) {
	// String string = new StringBuilder() .append("select distinct p.nome from ")
	// .append(Funcionario.class.getSimpleName() + " u ")
	// .append("inner join u.grupos g ")
	// .append("inner join g.permissoes p ")
	// .append("where u = :user")
	// .toString();
	//
	// return manager.createQuery(string, String.class).setParameter("user", user).getResultList();
	//
	// return new ArrayList<String>();
	// }

}
