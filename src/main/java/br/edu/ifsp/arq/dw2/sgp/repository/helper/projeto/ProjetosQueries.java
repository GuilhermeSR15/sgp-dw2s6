package br.edu.ifsp.arq.dw2.sgp.repository.helper.projeto;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.edu.ifsp.arq.dw2.sgp.model.Projeto;
import br.edu.ifsp.arq.dw2.sgp.repository.filter.ProjetosFilter;

public interface ProjetosQueries {
	public Page<Projeto> filtrar(ProjetosFilter filtro, Pageable pageable);
}
