package br.edu.ifsp.arq.dw2.sgp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.ifsp.arq.dw2.sgp.controller.page.PageWrapper;
import br.edu.ifsp.arq.dw2.sgp.model.Setor;
import br.edu.ifsp.arq.dw2.sgp.repository.Setores;
import br.edu.ifsp.arq.dw2.sgp.repository.filter.SetoresFilter;
import br.edu.ifsp.arq.dw2.sgp.service.SetorService;
import br.edu.ifsp.arq.dw2.sgp.service.exception.ImpossivelExcluirEntidadeException;
import br.edu.ifsp.arq.dw2.sgp.service.exception.NomeSetorJaCadastradoException;

@Controller
@RequestMapping("/setor")
public class SetoresController {

	@Autowired
	private Setores setores;
	
	@Autowired
	private SetorService setorService;


	@RequestMapping("/novo")
	public ModelAndView novo(Setor setor) {
		ModelAndView mv = new ModelAndView("setor/cadastro-setor");
		return mv;
	}

	@RequestMapping(value = "/novo", method = RequestMethod.POST)
	public ModelAndView cadastrar(@Valid Setor setor, BindingResult result, Model model,
			RedirectAttributes attributes) {
		if (result.hasErrors()) {
			return novo(setor);
		}
		try {
			setorService.salvar(setor);
		} catch (NomeSetorJaCadastradoException e) {
			result.rejectValue("nome", e.getMessage(), e.getMessage());
			return novo(setor);
		}

		attributes.addFlashAttribute("mensagem", "Setor salvo com sucesso");
		return new ModelAndView("redirect:/setor/novo");
	}

	@RequestMapping(value = "/pesquisar", method = RequestMethod.GET)
	public ModelAndView pesquisar(SetoresFilter setoresFilter, BindingResult result,
			@PageableDefault(size = 2) Pageable pageable, 
			HttpServletRequest httpServletRequest) {
		ModelAndView mv = new ModelAndView("setor/pesquisa-setor");

		PageWrapper<Setor> paginaWrapper = new PageWrapper<>(setores.filtrar(setoresFilter, pageable),
				httpServletRequest);
		mv.addObject("pagina", paginaWrapper);
		return mv;
	}
	
	@DeleteMapping("/{codigo}")
	public @ResponseBody ResponseEntity<?> excluir(@PathVariable("codigo") Setor setor) {
		try {
			setorService.excluir(setor);
		} catch (ImpossivelExcluirEntidadeException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(value = "/editar/{codigo}")
	public ModelAndView editar(@PathVariable("codigo") Setor setor) {
		ModelAndView mv = new ModelAndView("setor/cadastro-setor");
		mv.addObject(setorService.buscar(setor.getId()));
		return mv;
	}
	
	@RequestMapping(value = "/editar/{codigo}", method = RequestMethod.POST)
	public ModelAndView editar(@Valid Setor setor, BindingResult result, Model model,
			RedirectAttributes attributes) {
		if (result.hasErrors()) {
			return novo(setor);
		}
		try {
			setorService.salvar(setor);
		} catch (NomeSetorJaCadastradoException e) {
			result.rejectValue("nome", e.getMessage(), e.getMessage());
			return novo(setor);
		}
		attributes.addFlashAttribute("mensagem", "Setor alterado com sucesso");
		return new ModelAndView("redirect:/setor/novo");
	}

}
