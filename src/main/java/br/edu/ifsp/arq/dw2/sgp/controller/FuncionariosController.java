package br.edu.ifsp.arq.dw2.sgp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.ifsp.arq.dw2.sgp.controller.page.PageWrapper;
import br.edu.ifsp.arq.dw2.sgp.model.Funcionario;
import br.edu.ifsp.arq.dw2.sgp.repository.Funcionarios;
import br.edu.ifsp.arq.dw2.sgp.repository.filter.FuncionariosFilter;
import br.edu.ifsp.arq.dw2.sgp.service.FuncionarioService;
import br.edu.ifsp.arq.dw2.sgp.service.GrupoService;
import br.edu.ifsp.arq.dw2.sgp.service.SetorService;
import br.edu.ifsp.arq.dw2.sgp.service.exception.ConfirmSenhaObrigatoriaUsuarioException;
import br.edu.ifsp.arq.dw2.sgp.service.exception.EmailUsuarioJaCadastradoException;
import br.edu.ifsp.arq.dw2.sgp.service.exception.ImpossivelExcluirEntidadeException;
import br.edu.ifsp.arq.dw2.sgp.service.exception.SenhaObrigatoriaUsuarioException;

@Controller
@RequestMapping("/funcionario")
public class FuncionariosController {

	@Autowired
	private FuncionarioService funcionarioService;

	@Autowired
	private GrupoService grupoService;

	@Autowired
	private SetorService setorService;

	@Autowired
	private Funcionarios funcionarios;

	@GetMapping("/novo")
	public ModelAndView novo(Funcionario funcionario) {
		ModelAndView mv = new ModelAndView("funcionario/cadastro_funcionario");

		if (funcionario != null && (funcionario.getId() == null || funcionario.getId() == 0)) {
			funcionario.setAtivo(true);
		}

		mv.addObject("grupos", grupoService.listarTodos());
		mv.addObject("setores", setorService.listarTodos());
		return mv;
	}

	@PostMapping("/novo")
	public ModelAndView salvar(@Valid Funcionario funcionario, BindingResult result, RedirectAttributes attributes) {
		if (result.hasErrors()) {
			return novo(funcionario);
		}

		try {
			funcionario.setContentType("");
			funcionario.setFoto("");
			funcionarioService.salvar(funcionario);
		}
		catch (EmailUsuarioJaCadastradoException e) {
			result.rejectValue("email", e.getMessage(), e.getMessage());
			return novo(funcionario);
		}
		catch (SenhaObrigatoriaUsuarioException e) {
			result.rejectValue("senha", e.getMessage(), e.getMessage());
			return novo(funcionario);
		}

		attributes.addFlashAttribute("mensagem", "Funcionário salvo com sucesso");
		return new ModelAndView("redirect:/funcionario/novo");
	}

	@GetMapping("/pesquisar")
	public ModelAndView pesquisar(	FuncionariosFilter funcionariosFilter,
									BindingResult result,
									@PageableDefault(size = 2) Pageable pageable,
									HttpServletRequest httpServletRequest) {
		ModelAndView mv = new ModelAndView("funcionario/pesquisa-funcionarios");

		PageWrapper<Funcionario> paginaWrapper = new PageWrapper<>(funcionarios.filtrar(funcionariosFilter, pageable),
				httpServletRequest);
		
		mv.addObject("pagina", paginaWrapper);
		mv.addObject("grupos", grupoService.listarTodos());
		mv.addObject("setores", setorService.listarTodos());

		return mv;
	}

	@DeleteMapping("/{codigo}")
	public @ResponseBody ResponseEntity<?> excluir(@PathVariable("codigo") Funcionario funcionario) {
		try {
			funcionarioService.excluir(funcionario);
		}
		catch (ImpossivelExcluirEntidadeException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = "/editar/{codigo}")
	public ModelAndView editar(@PathVariable("codigo") Long codigo) {
		ModelAndView mv = new ModelAndView("funcionario/cadastro_funcionario");
		
		mv.addObject("funcionario", funcionarioService.buscar(codigo));
		mv.addObject("grupos", grupoService.listarTodos());
		mv.addObject("setores", setorService.listarTodos());
		
		return mv;
	}

	@RequestMapping(value = "/editar/{codigo}", method = RequestMethod.POST)
	public ModelAndView editar(	@Valid Funcionario funcionario,
								BindingResult result,
								Model model,
								RedirectAttributes attributes) {
		if (result.hasErrors()) {
			return novo(funcionario);
		}
		try {
			funcionarioService.salvar(funcionario);
		}
		catch (EmailUsuarioJaCadastradoException e) {
			result.rejectValue("email", e.getMessage(), e.getMessage());
			return novo(funcionario);
		}
		catch (SenhaObrigatoriaUsuarioException e) {
			result.rejectValue("senha", e.getMessage(), e.getMessage());
			return novo(funcionario);
		}
		catch (ConfirmSenhaObrigatoriaUsuarioException e) {
			result.rejectValue("confirmarSenha", e.getMessage(), e.getMessage());
			return novo(funcionario);
		}
		
		attributes.addFlashAttribute("mensagem", "Funcionário alterado com sucesso");

		return new ModelAndView("redirect:/funcionario/editar/" + funcionario.getId());
	}

}