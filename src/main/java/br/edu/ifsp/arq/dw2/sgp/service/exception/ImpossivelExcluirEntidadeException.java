package br.edu.ifsp.arq.dw2.sgp.service.exception;

public class ImpossivelExcluirEntidadeException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ImpossivelExcluirEntidadeException(String msg) {
		super(msg);
	}

}
