package br.edu.ifsp.arq.dw2.sgp.repository.helper.projeto_funcionario;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;

import br.edu.ifsp.arq.dw2.sgp.model.ProjetoFuncionario;
import br.edu.ifsp.arq.dw2.sgp.repository.filter.ProjetosFuncionariosFilter;

public class ProjetosFuncionariosImpl implements ProjetosFuncionariosQueries {

	@PersistenceContext
	private EntityManager manager;

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public Page<ProjetoFuncionario> filtrar(ProjetosFuncionariosFilter filtro, Pageable pageable) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(ProjetoFuncionario.class);

		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistro = paginaAtual * totalRegistrosPorPagina;

		criteria.setFirstResult(primeiroRegistro);
		criteria.setMaxResults(totalRegistrosPorPagina);

		Sort sort = pageable.getSort();
		if (sort != null) {
			Sort.Order order = sort.iterator().next();
			String property = order.getProperty();
			criteria.addOrder(order.isAscending() ? Order.asc(property) : Order.desc(property));
		}
		System.out.println(criteria.list());
		adicionarFiltro(filtro, criteria);
		return new PageImpl<>(criteria.list(), pageable, total(filtro));
	}

	private Long total(ProjetosFuncionariosFilter filtro) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(ProjetoFuncionario.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}

	private void adicionarFiltro(ProjetosFuncionariosFilter filtro, Criteria criteria) {
		if (filtro != null) {
			if (filtro.getId() != null) {
				criteria.add(Restrictions.eq("id", filtro.getId()));
			}

			if (filtro.getFuncionario() != null) {
				criteria.add(Restrictions.eq("id_funcionario", filtro.getFuncionario().getId()));
			}

			if (filtro.getProjeto() != null) {
				criteria.add(Restrictions.eq("id_projeto", filtro.getProjeto().getId()));
			}
		}
	}

}
