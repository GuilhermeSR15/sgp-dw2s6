package br.edu.ifsp.arq.dw2.sgp.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.PersistenceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.edu.ifsp.arq.dw2.sgp.model.Setor;
import br.edu.ifsp.arq.dw2.sgp.repository.Setores;
import br.edu.ifsp.arq.dw2.sgp.service.exception.ImpossivelExcluirEntidadeException;
import br.edu.ifsp.arq.dw2.sgp.service.exception.NomeSetorJaCadastradoException;

@Service
public class SetorService {
	@Autowired
	private Setores setores;

	@Transactional
	public Setor salvar(Setor setor) throws NomeSetorJaCadastradoException {
		Optional<Setor> estiloOptional = setores.findByNomeIgnoreCase(setor.getNome());
		if (estiloOptional.isPresent()) {
			throw new NomeSetorJaCadastradoException("Nome do setor já cadastrado");
		}

		return setores.saveAndFlush(setor);
	}
	
	public List<Setor> listarTodos() {
		return setores.findAll();
	}
	
	public Setor buscar(Long id) {
		return setores.findOne(id);
	}
	
	@Transactional
	public void excluir(Setor setor) {
		try {
			setores.delete(setor);
			setores.flush();
		} catch (PersistenceException e) {
			System.out.println(e);
			throw new ImpossivelExcluirEntidadeException(
				"Impossível apagar setor. Está vinculado a algum funcionário"
			);
		}
		
	}
}
