package br.edu.ifsp.arq.dw2.sgp.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "projeto_funcionario")
public class ProjetoFuncionario implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull(message = "Informe a carga horária")
	@Min(message = "Minimo 1 hora", value = 1)
    @Digits(integer=10, fraction=0, message = "Carga horária precisa ser um número inteiro")
	@Column(name = "carga_horaria", nullable=false)
	private Integer cargaHoraria;
	
	@NotNull(message = "Obrigatório informar se é gestor")
	@Column(nullable = false, columnDefinition = "TINYINT(1)")
	private Boolean gestor;
	
	@NotNull(message = "Informe a data de início!")
	@DateTimeFormat(pattern="dd/MM/yyyy")
	@Column(name = "inicio_participacao", nullable=false)
	private LocalDate inicioParticipacao;
	
	@NotNull(message = "Informe a data de término!")
	@DateTimeFormat(pattern="dd/MM/yyyy")
	@Column(name = "fim_participacao", nullable=false)
	private LocalDate fimParticipacao;

	@ManyToOne
	@NotNull(message = "Informe o projeto!")
	@JoinColumn(name = "id_projeto", nullable=false)
	private Projeto projeto;

	@ManyToOne
	@NotNull(message = "Informe o funcionário!")
	@JoinColumn(name = "id_funcionario", nullable=false)
	private Funcionario funcionario;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCargaHoraria() {
		return cargaHoraria;
	}

	public void setCargaHoraria(Integer cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}

	public Boolean getGestor() {
		return gestor;
	}

	public void setGestor(Boolean gestor) {
		this.gestor = gestor;
	}

	public LocalDate getInicioParticipacao() {
		return inicioParticipacao;
	}

	public void setInicioParticipacao(LocalDate inicioParticipacao) {
		this.inicioParticipacao = inicioParticipacao;
	}

	public LocalDate getFimParticipacao() {
		return fimParticipacao;
	}

	public void setFimParticipacao(LocalDate fimParticipacao) {
		this.fimParticipacao = fimParticipacao;
	}

	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProjetoFuncionario other = (ProjetoFuncionario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


	
}