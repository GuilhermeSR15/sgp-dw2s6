package br.edu.ifsp.arq.dw2.sgp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.ifsp.arq.dw2.sgp.model.Grupo;
import br.edu.ifsp.arq.dw2.sgp.repository.helper.grupo.GruposQueries;

@Repository
public interface Grupos extends JpaRepository<Grupo, Long>, GruposQueries {
	public Optional<Grupo> findByNomeIgnoreCase(String nome);
}
