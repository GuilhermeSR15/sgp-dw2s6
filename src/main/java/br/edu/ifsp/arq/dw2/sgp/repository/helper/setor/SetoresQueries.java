package br.edu.ifsp.arq.dw2.sgp.repository.helper.setor;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.edu.ifsp.arq.dw2.sgp.model.Setor;
import br.edu.ifsp.arq.dw2.sgp.repository.filter.SetoresFilter;

public interface SetoresQueries {
	public Page<Setor> filtrar(SetoresFilter filtro, Pageable pageable);
}
