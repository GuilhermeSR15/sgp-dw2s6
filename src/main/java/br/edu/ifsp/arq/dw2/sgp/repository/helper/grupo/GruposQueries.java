package br.edu.ifsp.arq.dw2.sgp.repository.helper.grupo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.edu.ifsp.arq.dw2.sgp.model.Grupo;
import br.edu.ifsp.arq.dw2.sgp.repository.filter.GruposFilter;

public interface GruposQueries {
	public Page<Grupo> filtrar(GruposFilter filtro, Pageable pageable);
}
