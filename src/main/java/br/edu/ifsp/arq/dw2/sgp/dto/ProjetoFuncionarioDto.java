package br.edu.ifsp.arq.dw2.sgp.dto;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import br.edu.ifsp.arq.dw2.sgp.model.Funcionario;
import br.edu.ifsp.arq.dw2.sgp.model.Projeto;

public class ProjetoFuncionarioDto {
	private Long id;
	
	@NotNull(message = "Informe a carga horária")
	@Min(message = "Minimo 1 hora", value = 1)
    @Digits(integer=10, fraction=0, message = "Carga horária precisa ser um número inteiro")
	private Integer cargaHoraria;
	@NotNull(message = "Obrigatório informar se é gestor")
	@Column(nullable = false, columnDefinition = "TINYINT(1)")
	private Boolean gestor;
	@NotNull(message = "Informe a data de início!")
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private LocalDate inicioParticipacao;
	@NotNull(message = "Informe a data de término!")
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private LocalDate fimParticipacao;
	@NotNull(message = "Informe o projeto!")
	private Projeto projeto;
	@NotNull(message = "Informe pelo menos um funcionário!")
	private List<Funcionario> funcionarios;
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getCargaHoraria() {
		return cargaHoraria;
	}
	public void setCargaHoraria(Integer cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}
	public Boolean getGestor() {
		return gestor;
	}
	public void setGestor(Boolean gestor) {
		this.gestor = gestor;
	}
	public LocalDate getInicioParticipacao() {
		return inicioParticipacao;
	}
	public void setInicioParticipacao(LocalDate inicioParticipacao) {
		this.inicioParticipacao = inicioParticipacao;
	}
	public LocalDate getFimParticipacao() {
		return fimParticipacao;
	}
	public void setFimParticipacao(LocalDate fimParticipacao) {
		this.fimParticipacao = fimParticipacao;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public List<Funcionario> getFuncionarios() {
		return funcionarios;
	}
	public void setFuncionarios(List<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}
	
	
}
