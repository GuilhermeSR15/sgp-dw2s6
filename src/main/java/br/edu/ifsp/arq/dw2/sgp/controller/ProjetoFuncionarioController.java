package br.edu.ifsp.arq.dw2.sgp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.ifsp.arq.dw2.sgp.controller.page.PageWrapper;
import br.edu.ifsp.arq.dw2.sgp.dto.ProjetoFuncionarioDto;
import br.edu.ifsp.arq.dw2.sgp.model.ProjetoFuncionario;
import br.edu.ifsp.arq.dw2.sgp.repository.ProjetosFuncionarios;
import br.edu.ifsp.arq.dw2.sgp.repository.filter.ProjetosFuncionariosFilter;
import br.edu.ifsp.arq.dw2.sgp.service.FuncionarioService;
import br.edu.ifsp.arq.dw2.sgp.service.ProjetoFuncionarioService;
import br.edu.ifsp.arq.dw2.sgp.service.ProjetoService;
import br.edu.ifsp.arq.dw2.sgp.service.exception.FuncionarioJaAlocadoNoProjeto;

@Controller
@RequestMapping("/projeto-funcionario")
public class ProjetoFuncionarioController {
	@Autowired
	private ProjetoService projetoService;

	@Autowired
	private ProjetoFuncionarioService projetoFuncionarioService;
	
	@Autowired
	private FuncionarioService funcionarioService;
	
	@Autowired
	private ProjetosFuncionarios projetosFuncionarios;

	@RequestMapping("/novo")
	public ModelAndView novo(ProjetoFuncionarioDto projetoFuncionarioDto) {
		ModelAndView mv = new ModelAndView("projeto-funcionario/cadastro-projeto-funcionario");
		mv.addObject("projetos", projetoService.listarTodos());
		mv.addObject("funcionarios", funcionarioService.listarTodos());
		return mv;
	}

	@RequestMapping(value = "/novo", method = RequestMethod.POST)
	public ModelAndView cadastrar(@Valid ProjetoFuncionarioDto projetoFuncionarioDto, BindingResult result, Model model,
			RedirectAttributes attributes) {
		if (result.hasErrors()) {
			return novo(projetoFuncionarioDto);
		}
		try {
			projetoFuncionarioService.salvar(projetoFuncionarioDto);
		} catch (FuncionarioJaAlocadoNoProjeto e) {
			result.rejectValue("funcionarios", e.getMessage(), e.getMessage());
			return novo(projetoFuncionarioDto);
		}
		attributes.addFlashAttribute("mensagem", "Funcionários alocados  com sucesso");
		return new ModelAndView("redirect:/projeto-funcionario/novo");
	}
	
	@GetMapping("/pesquisar")
	public ModelAndView pesquisar(	ProjetosFuncionariosFilter projetosFuncionariosFilter,
									BindingResult result,
									@PageableDefault(size = 2) Pageable pageable,
									HttpServletRequest httpServletRequest) {
		ModelAndView mv = new ModelAndView("/projeto-funcionario/pesquisa-projeto-funcionario");

		PageWrapper<ProjetoFuncionario> paginaWrapper = new PageWrapper<>(projetosFuncionarios.filtrar(projetosFuncionariosFilter, pageable),
				httpServletRequest);
		
		mv.addObject("pagina", paginaWrapper);
		mv.addObject("projetos", projetoService.listarTodos());
		mv.addObject("funcionarios", funcionarioService.listarTodos());

		return mv;
	}
}
