package br.edu.ifsp.arq.dw2.sgp.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import br.edu.ifsp.arq.dw2.sgp.validation.AtributoConfirmacao;

@Entity
@Table(name = "funcionario")
@AtributoConfirmacao(atributo = "senha", atributoConfirmacao = "confirmacaoSenha", message = "Confirmação da senha não confere")
public class Funcionario implements Serializable {

	private static final long serialVersionUID = 1L;

	public Funcionario() {
	}

	public Funcionario(long id) {
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank(message = "Informe o nome do funcionário!")
	private String nome;

	@NotBlank(message = "Informe o CPF do funcionário!")
	@Size(min = 14, max = 14, message = "O CPF deve conter 14 caracteres!")
	private String cpf;

	@NotBlank(message = "Informe o e-mail do funcionário!")
	@Email(message = "E-mail é inválido!")
	private String email;

	@NotNull(message = "Informe o salário do funcionário!")
	@DecimalMin(value = "0.1", message = "Salário deve ser maior que R$0,50")
	@DecimalMax(value = "999999.99", message = "Salário deve ser menor que R$999.999,99")
	private BigDecimal salario;

	@NotNull(message = "Informe o status do funcionário!")
	private Boolean ativo;

	private String foto;

	@Column(name = "content_type")
	private String contentType;

	@ManyToOne
	@JoinColumn(name = "id_setor", nullable = false)
	private Setor setor;

	@ManyToOne
	@JoinColumn(name = "id_grupo", nullable = false)
	private Grupo grupo;

	@Column(name = "nome_usuario")
	private String nomeUsuario;

	private String senha;

	@Transient
	// @NotBlank(message = "Confirmar senha é obrigatório")
	private String confirmacaoSenha;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "projeto", cascade = CascadeType.ALL)
	private List<ProjetoFuncionario> projetoFuncionario;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public BigDecimal getSalario() {
		return salario;
	}

	public void setSalario(BigDecimal salario) {
		this.salario = salario;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Setor getSetor() {
		return setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Grupo getGrupo() {
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	public List<ProjetoFuncionario> getProjetoFuncionario() {
		return projetoFuncionario;
	}

	public void setProjetoFuncionario(List<ProjetoFuncionario> projetoFuncionario) {
		this.projetoFuncionario = projetoFuncionario;
	}

	public String getConfirmacaoSenha() {
		return confirmacaoSenha;
	}

	public void setConfirmacaoSenha(String confirmacaoSenha) {
		this.confirmacaoSenha = confirmacaoSenha;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcionario other = (Funcionario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public boolean isNovo() {
		return id == null;
	}

}
