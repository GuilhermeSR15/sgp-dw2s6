package br.edu.ifsp.arq.dw2.sgp.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "projeto")
public class Projeto implements Serializable {

	private static final long serialVersionUID = 1L;

	public Projeto() {

	}

	public Projeto(long id) {
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank(message = "Informe o nome do projeto!")
	@Size(max = 100, message = "O nome do projeto não pode ter mais de 100 caracteres!")
	private String nome;

	@NotBlank(message = "Informe a descrição do projeto!")
	private String descricao;

	@NotNull(message = "Informe a data de início!")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate inicio;

	@NotNull(message = "Informe a data de término!")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate fim;

	@Column(nullable = false, columnDefinition = "TINYINT(1)")
	private Boolean ativo;

	@ManyToOne
	@JoinColumn(name = "id_setor", nullable = false)
	private Setor setor;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "projeto", cascade = CascadeType.ALL)
	private List<ProjetoFuncionario> projetoFuncionario;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public LocalDate getInicio() {
		return inicio;
	}

	public void setInicio(LocalDate inicio) {
		this.inicio = inicio;
	}

	public LocalDate getFim() {
		return fim;
	}

	public void setFim(LocalDate fim) {
		this.fim = fim;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Setor getSetor() {
		return setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}

	public List<ProjetoFuncionario> getProjetoFuncionario() {
		return projetoFuncionario;
	}

	public void setProjetoFuncionario(List<ProjetoFuncionario> projetoFuncionario) {
		this.projetoFuncionario = projetoFuncionario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Projeto other = (Projeto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (this.ativo == null) {
			this.ativo = true;
		}
	}

}
