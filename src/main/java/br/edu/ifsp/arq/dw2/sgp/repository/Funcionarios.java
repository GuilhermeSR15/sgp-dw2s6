package br.edu.ifsp.arq.dw2.sgp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.ifsp.arq.dw2.sgp.model.Funcionario;
import br.edu.ifsp.arq.dw2.sgp.repository.helper.funcionario.FuncionariosQueries;

@Repository
public interface Funcionarios extends JpaRepository<Funcionario, Long>, FuncionariosQueries {
	public Optional<Funcionario> findByEmail(String email);

}
