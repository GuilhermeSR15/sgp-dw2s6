package br.edu.ifsp.arq.dw2.sgp.controller.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.util.StringUtils;

import br.edu.ifsp.arq.dw2.sgp.model.Setor;

public class SetorConverter implements Converter<String, Setor>{

	@Override
	public Setor convert(String codigo) {
		System.out.println("Codigo: " + codigo);
		if (!StringUtils.isEmpty(codigo)) {
			Setor setor = new Setor();
			setor.setId(Long.valueOf(codigo));
			return setor;
		}
	
		return null;
	}

}